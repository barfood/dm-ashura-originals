Stepmania DM Ashura Pack Changelogs
____________________________________________
v0.7 Changelog

New Music:
Diamond Dust
MaxX Borg Assimilation
Rave Until The Night Is Over (feat. MC Jay & Veronica)
Your Angel
aftershock!!
Metropolis

New Steps:
Diamond Dust [BEG-HVY]
Rave Until The Night Is Over (feat. MC Jay & Veronica) [CHA]
Your Angel [BEG-HVY]
Metropolis [BEG-HVY]
MaxX Borg Assimilation [CHA]

Completed Stepcharts:
Ice Cap Zone (Glacial Mix) [now CHA only]
Survivor Showdown [now BEG-HVY]

Revised Stepcharts:
Rave Until The Night Is Over [STD,HVY]

New Graphics:
MaxX Borg Assimilation
Diamond Dust
Rave Until The Night Is Over (feat. MC Jay & Veronica)
Your Angel
Metropolis

New BG Movies:
aftershock!!
Rave Until The Night Is Over (feat. MC Jay & Veronica)
Metropolis
Your Angel
MaxX Borg Assimilation

New Simfiles:
aftershock!! [Universe 3 Simfile]
deltaMAX (Universe edit) [Universe 3 Simfile]
Moonlight Rave (165 BPM Ver.)
Z (DTX Version)
Nautilus (Long Version)

Other Important Stuff:
____________________________________________
v0.6 Changelog

New Music:

New Steps:
Ice Cap Zone (Glacial Mix) [HVY]

Completed Stepcharts:
Euphorium [now BEG-HVY]
GO! [now BEG-HVY]

New Graphics:
Euphorium
GO!
Rave Until The Night Is Over
Jaunt (Midgaral Mix)
Ice Cap Zone (Glacial Mix)

New BG Movies:
Anubis (Black Land Mix)
Celebrate Nite (Like It's '99 Mix)
Euphorium
Dies Irae
KlungKung 2004
Rave Until The Night Is Over
Vanity Angel -Seraphim Mix-
Vampire Killer (Neo-Thunder Mix)
Genom Screams (Virus Mix)
Ice Cap Zone (Glacial Mix)
Jaunt (Midgaral Mix)
Final Audition (Omega Mix)
Otter's Dance (Hotroot Mix)
Psi^2
R3 (Omega Mix)
XenomaX
neoMAX (full version)
Exotic Ethnic (Neo-Shiva Mix)

New Simfiles:

Other Important Stuff:
Fixed MaxX ResurrexXion's graphics
Fixed XenomaX's stepcharts
Slightly modified Z and it's BG Movie
Synched Nautilus correctly
Resized Seven's graphics
Slightly Modified Moonlight Rave
Fixed small sync issue with Omega

____________________________________________
v0.5 Changelog

New Music:
Ice Cap Zone (Glacial Mix)
Jaunt (Midgaral Mix)
Rave Until The Night Is Over
Nagi
Psychosis

New Steps:
Rave Until The Night Is Over [BEG-HVY]
Jaunt (Midgaral Mix) [BEG-HVY]

Completed Stepcharts:
Nautilus [now BEG-ONI]
Neon Rush [now BEG-HVY]
Hydrocity Zone Act 1 [now BEG-HVY]
DXY^2!! [now BEG-HVY]

New Graphics:
Hydrocity Zone Act 1
Nautilus
Neon Rush
Fantasy Impromptu

New BG Movies:
Boss Machine
Classical Insanity
DXY^2!!
GO!
Hydrocity Zone Act 1
Nautilus
Neon Rush
Omega
Fantasy Impromptu
Moonlight Rave

New Simfiles:
Final Audition (Omega Mix)

____________________________________________
v0.4 Changelog

New Music:

New Steps:
DXY^2!! [HVY]
Euphorium [HVY]
Hydrocity Zone Act 1 [HVY]
Nautilus [HVY]
Neon Rush [HVY]
GO! [HVY]

Completed Stepcharts:
Omega [now BEG-ONI]

New Graphics:

New BG Movies:
AAA
Symbolic (Cryptex Mix)
Aztec Templing (Techno-titlan Mix)
neoMAX

New Simfiles:
Moonlight Rave
Exotic Ethnic (Neo-Shiva Mix)

____________________________________________
v0.3 Changelog

New Music:

New Steps:

Completed Stepcharts:

New Graphics:

New BG Movies:
Chaotic WHITE
Astral
neoMAX
Seven
GO! (Mahalo Mix)
Survivor Showdown

New Simfiles:
Symbolic (Cryptex Mix)

____________________________________________
v0.2 Changelog

New Music:
GO!
Euphorium
Neon Rush
Hydrocity Zone Act 1
Obelisk
Nautilus
DXY^2!!

New Steps:

Completed Stepcharts:

New Graphics:

New BG Movies:
MAX Forever
Z

New Simfiles:
Omega
Survivor Showdown

____________________________________________